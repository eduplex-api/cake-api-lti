<?php

declare(strict_types = 1);

namespace Lti\Controller;

use App\Controller\ApiController;
use App\Model\Table\UsersTable;
use Cake\Controller\ComponentRegistry;
use Cake\Event\EventManagerInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Exception\NotImplementedException;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Firebase\JWT\JWT;
use Lti\Lib\Config\PlatformConfig;
use Lti\Lib\PrivateKey;
use Lti\LtiPlugin;

/**
 * @property UsersTable $Users
 * @property \App\Lib\Helpers\CookieHelper $CookieHelper
 */
class PlatformLoginController extends ApiController
{
    public const RESOURCE_ID = 'beaea819-3e30-49f3-b730-42d4a4635f83';//'7b3c5109-b402-4eac-8f61-bdafa301cbb4';
    public const PLATFORM_JWT_KEY_ID = '5c02c00a-12fa-4574-905c-d62d0791c8d8';//'fcec4f14-28a5-4697-87c3-e9ac361dada5';

    public function __construct(
        $cookieHelper,
        ?ServerRequest $request = null,
        ?Response $response = null,
        ?string $name = null,
        ?EventManagerInterface $eventManager = null,
        ?ComponentRegistry $components = null
    ) {
        $this->CookieHelper = $cookieHelper;
        parent::__construct($request, $response, $name, $eventManager, $components);
    }

    public function isPublicController(): bool
    {
        return false;
    }

    public function initialize(): void
    {
        parent::initialize();
        $this->Users = UsersTable::load();
        $this->_getAccessTokenFromCookie();
    }

    private function _getAccessTokenFromCookie()
    {
        $tokenFromCookie = $this->CookieHelper->readApi2Remember($this->getRequest());
        //$tokenFromCookie = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';// TODO hard code token in dev
        if ($tokenFromCookie) {
            $_SERVER['HTTP_AUTHORIZATION'] = 'Bearer ' . $tokenFromCookie;
            $this->request = $this->getRequest()->withEnv('HTTP_AUTHORIZATION', $_SERVER['HTTP_AUTHORIZATION']);
        }
        return $tokenFromCookie;
    }

    public static function domain(): string
    {
        //return 'https://proto.eduplex.eu';
        $domain = $_SERVER['HTTP_HOST'] ?? 'host';
        return "https://$domain";
    }

    public static function host(): string
    {
        return self::domain() . '/api/v3/lti/platform';
    }

    public static function iss(): string
    {
        //return 'https://proto.eduplex.eu/api/v3/lti/platform';
        return self::host();
    }

    protected function getList()
    {
        $request = $this->getRequest();
        $userID = $this->OAuthServer->getUserID();
        $context = $this->Users->getForLti($userID, $request->getQuery('lti_message_hint'));
        $formAction = $request->getQuery('redirect_uri');
        $defaultState = $request->getQuery('state');
        $clientId = $request->getQuery('client_id');
        $responseMode = $request->getQuery('response_mode');
        if (!$responseMode) {
            throw new BadRequestException('response_mode parameter is mandatory');
        }
        if ($responseMode != 'form_post') {
            throw new NotImplementedException('Response mode not implemented ' . $responseMode);
        }
        if (!PlatformConfig::isToolClientIdAllowed($clientId)) {
            throw new InternalErrorException('The platform is not configured to use this tool client_id');
        }
        $jwt = $this->_buildJWT($clientId, $request->getQuery('nonce'), $context);
        $params = [
            'state' => $defaultState,
            'id_token' => $jwt,
        ];
        $redirect = $formAction . '?' . http_build_query($params);
        if (PlatformConfig::hasAntiSpecRedirectionUrlAsGet($clientId)) {
            return $this->redirect($redirect);
        }
        $html = '<form id="auto_submit" action="'.$formAction.'" method="POST" style="display: none;">';
        $html .= $redirect . ' <br><br>';
        $html .= '<input name="id_token" value="'.$jwt.'" style="width: 100%;" /><br>
            <input name="state" value="'.$defaultState.'" style="width: 100%;" /><br>
            <button type="submit">Submit from PlatformLoginController</button>
        </form><script>document.getElementById("auto_submit").submit();</script>';
        $this->response = $this->response->withStringBody($html);
        $this->return = $this->response;
        //*/
    }

    private function _buildJWT(string $clientId, string $nonce, array $context): string
    {
        $config = PlatformConfig::getConfigFromClientId($clientId);
        $target_link_uri = $config->getTargetLinkUrl();
        //$target_link_uri = ConfigPanoptoFhtlc::getFirstRedirectionUrl();// requested by panopto
        $lineItemsUri = self::host() . '/' . LtiPlugin::PLATFORM_LINE_ITEMS;
        $nrpsUri = self::host().'/'.LtiPlugin::PLATFORM_NRPS;//"http://localhost/platform/services/nrps";
        $deploymentId = PlatformConfig::getFirstDeploymentId($clientId);
        $iss = PlatformLoginController::iss();
        $userId = $context['sub'] ?? PlatformNrpsController::USER_ID1;
        $message_jwt = [
            'iss' => $iss,
            'aud' => [$clientId],
            'sub' => $userId,
            'exp' => time() + 600,
            'iat' => time(),
            'nonce' => $nonce,
            'https://purl.imsglobal.org/spec/lti/claim/deployment_id' => $deploymentId,
            'https://purl.imsglobal.org/spec/lti/claim/message_type' => 'LtiResourceLinkRequest',
            'https://purl.imsglobal.org/spec/lti/claim/version' => '1.3.0',
            'https://purl.imsglobal.org/spec/lti/claim/target_link_uri' => $target_link_uri,
            'https://purl.imsglobal.org/spec/lti/claim/resource_link' => [
                'id' => PlatformLoginController::RESOURCE_ID,
                //"title" => "Phone home",
                //"description" => "Will ET phone home, or not; click to discover more."
            ],
            'https://purl.imsglobal.org/spec/lti-nrps/claim/namesroleservice' => [
                'context_memberships_url' => $nrpsUri,
                'service_versions' => [
                    '2.0',
                    //"1.0",
                ]
            ],
            'https://purl.imsglobal.org/spec/lti-ags/claim/endpoint' => [
                'scope' => [
                    'https://purl.imsglobal.org/spec/lti-ags/scope/lineitem',
                    //"https://purl.imsglobal.org/spec/lti-ags/scope/lineitem.readonly",
                    'https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly',
                    'https://purl.imsglobal.org/spec/lti-ags/scope/score'
                ],
                'lineitems' => $lineItemsUri,
            ],
        ];
        $message_jwt = array_merge($context, $message_jwt);
        //$message_jwt['https://purl.imsglobal.org/spec/lti/claim/ext'] = [];
        //$message_jwt['https://purl.imsglobal.org/spec/lti/claim/lis'] = [];
        //$message_jwt['https://purl.imsglobal.org/spec/lti/claim/context'] = [
        //    "id" => "10",
        //    "label" => "achilla",
        //    "title" => "MD Achilla",
        //    "type" => [
        //        "CourseSection",
        //    ],
        //];
        //$message_jwt['https://purl.imsglobal.org/spec/lti/claim/tool_platform'] = [];
        //$message_jwt['https://purl.imsglobal.org/spec/lti/claim/launch_presentation'] = [];
        //$message_jwt['https://purl.imsglobal.org/spec/lti-bo/claim/basicoutcome'] = [];
        //$database = new \Lti\Lib\ToolDatabase();
        return JWT::encode(
            $message_jwt,
            PrivateKey::private_key($iss),
            'RS256',
            PlatformLoginController::PLATFORM_JWT_KEY_ID
        );
    }
}
