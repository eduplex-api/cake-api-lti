<?php

declare(strict_types = 1);

namespace Lti\Test\TestCase\Controller;

use App\Test\Fixture\OauthAccessTokensFixture;
use Lti\Lib\Config\Platform\ConfigDemoGame;
use Lti\LtiPlugin;
use RestApi\TestSuite\ApiCommonErrorsTest;

class CustomServiceToMock {
    // phpcs:ignore
    public function readApi2Remember() {}
}

class PlatformLoginControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return LtiPlugin::getRoutePath() . '/lti/platform/' . LtiPlugin::PLATFORM_LOGIN . '/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->_mockService();
    }

    private function _mockService()
    {
        $mock = $this->createMock(CustomServiceToMock::class);
        $mock->expects($this->once())->method('readApi2Remember')
            ->willReturn(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $this->mockService(\App\Lib\Helpers\CookieHelper::class, function () use ($mock) {
            return $mock;
        });
    }

    public function testGetList()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);

        $params = [
            'response_mode' => 'form_post',
            'nonce' => 'mynonce',
            'redirect_uri' => 'https://dev.courseticket.com/api/v3/lti/tool/gamephp/',
            'state' => 'one2',
            'client_id' => ConfigDemoGame::TOOL_CLIENT_ID,
        ];
        $this->get($this->_getEndpoint().'?'.http_build_query($params));

        $expected = 'https://dev.courseticket.com/api/v3/lti/tool/gamephp/?state=one2&id_token=';
        $this->assertRedirectContains($expected, $this->_getBodyAsString());
        $this->assertEquals(302, $this->_response->getStatusCode());
    }

    public function testGetList_withGoodHabitz()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);

        $state = 'state-8003e375-1c94-4d3b-8e34-4b603152da04';
        $params = [
            'scope' => 'openid',
            'response_type' => 'id_token',
            'response_mode' => 'form_post',
            'prompt' => 'none',
            'client_id' => 'b6b6b653-832a-442a-886f-a194b76c619e',
            'redirect_uri' => 'https%3A%2F%2Flti13.goodhabitz.com%2Flaunch%2F',
            'state' => $state,
            'nonce' => '039a909f929445b289c684213136e1931a78eca21cfd11ef85a16a12b4403af8',
            'login_hint' => '12345',
            'lti_message_hint' => '12345',
        ];
        $this->get($this->_getEndpoint().'?'.http_build_query($params));

        $this->assertEquals(200, $this->_response->getStatusCode());
        $jwt = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjVjMDJjMDBhLTEyZmEtNDU3NC05MDVjLWQ2MmQwNzkxYzhkOCJ9.';
        $expected = '<form id="auto_submit" action="https%3A%2F%2Flti13.goodhabitz.com%2Flaunch%2F" method="POST" style="display: none;">'
            . "https%3A%2F%2Flti13.goodhabitz.com%2Flaunch%2F?state=$state&id_token=$jwt";
        $this->assertStringStartsWith($expected, $this->_getBodyAsString());
    }
}
