<?php

namespace Lti\Lib\Config\Platform;

class ConfigGoodHabitz implements PlatformConfiguration
{
    private const TOOL_CLIENT_ID = 'b6b6b653-832a-442a-886f-a194b76c619e';
    private const TOOL_DEPLOYMENT_ID = '16b981d3-2dc3-4cb8-a2b6-3252067cef92';
    private const TOOL_LOGIN_URL = 'https://lti13.goodhabitz.com/login/';
    private const TOOL_REDIRECTION_URL = 'https://lti13.goodhabitz.com/launch/';
    private const TOOL_KEYSET_URL = 'https://lti13.goodhabitz.com/jwks/';

    public static function getLoginUrl(): string
    {
        return self::TOOL_LOGIN_URL;
    }

    public static function usePostInitialization(): bool
    {
        return false;
    }

    public static function getFirstRedirectionUrl(): string
    {
        return self::TOOL_REDIRECTION_URL;
    }

    public static function getPublicKeySetUrl(): string
    {
        return self::TOOL_KEYSET_URL;
    }

    public static function getDeploymentIDs(): array
    {
        return [self::TOOL_DEPLOYMENT_ID];
    }

    public static function getClientID(): string
    {
        return self::TOOL_CLIENT_ID;
    }

    public static function getTargetLinkUrl(): string
    {
        // GoodHabitz with getLoginUrl() instead of getFirstRedirectionUrl()
        //return self::getFirstRedirectionUrl();
        // test maybe login link instead of lunch link
        return self::getLoginUrl();
    }
}
