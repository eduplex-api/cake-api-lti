<?php

declare(strict_types = 1);

namespace Lti\Controller;

use App\Controller\ApiController;
use Lti\LtiPlugin;

/**
 * Names and Role Provisioning Services
 *
 * The Learning Tools Interoperability (LTI)® Names and Role Provisioning Services
 * is an LTI™ specification for providing access to a list of users and their roles
 * within context of a course, program or other grouping. The LTI™ specification
 * enables instructors to automate the provision of student lists via LTI to an
 * external tool. LTI does not pass user information in its default configuration.
 * Using the LTI Names and Role Provisioning Services, user information can be passed
 * in a safe and secure manner. The Names and Role Provisioning Services also allows
 * instructors to be provided a display showing the activity of all of their students,
 * whether or not they have accessed the tools. An earlier iteration of this spec was
 * formerly called LTI Membership Services.
 */
class PlatformNrpsController extends ApiController
{
    public const USER_ID1 = '0ae836b9-7fc9-4060-006f-27b2066ac545';
    public const UID2 = '4d0b3941-83f5-47fe-bd8a-66b39aa0651d';
    private const CONTEXT = '2923-abc';

    public function isPublicController(): bool
    {
        return false;
    }

    protected function getList()
    {
        $this->flatResponse = true;
        $this->return = [
            'id' => PlatformLoginController::host().'/'.LtiPlugin::PLATFORM_NRPS,
            'members' => [
                [
                    'status' => 'Active',
                    'context_id' => self::CONTEXT,
                    'name' => 'Ramon Senaida',
                    'given_name' => 'Ramon',
                    'family_name' => 'Senaida',
                    'user_id' => PlatformNrpsController::USER_ID1,
                    'roles' => [
                        'Instructor' // http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor
                    ],
                    'message' => []
                ],
                [
                    'status' => 'Active',
                    'context_id' => self::CONTEXT,
                    'name' => 'Marget '.PlatformLoginController::domain(),
                    'given_name' => 'Marget',
                    'family_name' => PlatformLoginController::domain(),
                    'user_id' => PlatformNrpsController::UID2,
                    'roles' => [
                        'Instructor' // http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor
                    ],
                    'message' => []
                ]
            ]
        ];
    }
}
