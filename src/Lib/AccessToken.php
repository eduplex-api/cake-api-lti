<?php

namespace Lti\Lib;

class AccessToken extends \OAuth2\ResponseType\AccessToken
{
    private const CLIENT_ID = 'lti13aaa-3b21-49fc-ae96-c21d27ebc037';

    public function storeAccessToken(string $scope, $userId = null): array
    {
        $token = [
            'access_token' => $this->generateAccessToken(),
            'expires_in' => $this->config['access_lifetime'],
            'token_type' => $this->config['token_type'],
            'scope' => $scope
        ];

        $expires = $this->config['access_lifetime'] ? time() + $this->config['access_lifetime'] : null;
        $this->tokenStorage->setAccessToken($token['access_token'], self::CLIENT_ID, $userId, $expires, $scope);

        return $token;
    }
}
