<?php

declare(strict_types = 1);

namespace Lti;

use Cake\Routing\RouteBuilder;
use Lti\Controller\PlatformLoginController;
use RestApi\Lib\RestPlugin;

class LtiPlugin extends RestPlugin
{
    public const PLATFORM_LOGIN = 'login';
    public const PLATFORM_POST_TOOL = 'post_tool';
    public const PLATFORM_JWKS = 'jwks';
    public const TOOL_JWKS = 'jwks';
    public const TOOL_LOGIN = 'login';
    public const TOOL_SCOREBOARD = 'api/scoreboard';
    public const TOOL_SCORE = 'api/score';
    public const PLATFORM_AGS = 'services/ags';
    public const PLATFORM_LINE_ITEMS = 'services/ags/lineitems';
    public const PLATFORM_NRPS = 'services/nrps';
    public const PLATFORM_TOKEN = 'token';
    public const TOOL_REDIRECTION = 'game-callback';

    protected function routeConnectors(RouteBuilder $builder): void
    {
        $platform = '/lti/platform/';
        $builder->connect($platform.self::PLATFORM_JWKS.'/*', \Lti\Controller\PlatformJwksController::route());
        $builder->connect($platform.self::PLATFORM_POST_TOOL.'/*', \Lti\Controller\PlatformPostToolController::route());
        $builder->connect($platform.self::PLATFORM_TOKEN.'/*', \Lti\Controller\PlatformTokenController::route());
        $builder->connect($platform.'*', \Lti\Controller\PlatformIframeController::route());
        $builder->connect($platform.self::PLATFORM_NRPS.'/*', \Lti\Controller\PlatformNrpsController::route());
        $builder->connect('/lti/platform/'.self::PLATFORM_LOGIN.'/*', PlatformLoginController::route());

        $builder->connect($platform.self::PLATFORM_LINE_ITEMS.'/*',
            \Lti\Controller\PlatformLineitemsController::route());

        $builder->connect($platform.self::PLATFORM_AGS.'/*', \Lti\Controller\PlatformAgsController::route());
        $builder->connect($platform.self::PLATFORM_AGS.'/results/*', \Lti\Controller\PlatformAgsController::route());
        $builder->connect($platform.self::PLATFORM_AGS.'/scores/*', \Lti\Controller\PlatformAgsController::route());

        $builder->connect('/lti/tool/'.self::TOOL_LOGIN.'/*', \Lti\Controller\ToolLoginController::route());
        $builder->connect('/lti/tool/'.self::TOOL_REDIRECTION.'/*', \Lti\Controller\ToolGameController::route());
        $builder->connect('/lti/tool/'.self::TOOL_JWKS.'/*', \Lti\Controller\ToolJwksController::route());
        $builder->connect('/lti/tool/'.self::TOOL_SCOREBOARD.'/*', \Lti\Controller\ToolScoreboardController::route());
        $builder->connect('/lti/tool/'.self::TOOL_SCORE.'/*', \Lti\Controller\ToolScoreController::route());
        $builder->connect('/lti/tool/static/*', \Lti\Controller\ToolStaticController::route());
        $builder->connect('/lti/openapi/*', \Lti\Controller\SwaggerJsonController::route());
    }

    public function services(\Cake\Core\ContainerInterface $container): void
    {
        $container->addShared(\App\Lib\Helpers\CookieHelper::class);// addShared means singleton
        $container->add(PlatformLoginController::class)
            ->addArguments([\App\Lib\Helpers\CookieHelper::class, \Cake\Http\ServerRequest::class]);
    }
}
