<?php

declare(strict_types = 1);

namespace Lti\Controller;

use Lti\LtiPlugin;

class SwaggerJsonController extends \RestApi\Controller\SwaggerJsonController
{
    protected function getContent(\RestApi\Lib\Swagger\SwaggerReader $reader, array $paths): array
    {
        $host = $_SERVER['HTTP_HOST'] ?? 'example.com';
        $plugin = LtiPlugin::getRoutePath();
        $url = 'https://' . $host . $plugin . '/';
        return [
            'openapi' => '3.0.0',
            'info' => [
                'version' => '0.1.12',
                'title' => 'LTI plugin',
                'description' => 'LTI connector plugin',
                'termsOfService' => 'https://www.eduplex.eu/impressum/',
                'contact' => [
                    'email' => 'adrian+eduplex_lti_oas@courseticket.com'
                ],
            ],
            'servers' => [
                ['url' => $url]
            ],
            'tags' => [],
            'paths' => $paths,
            'components' => [
                'securitySchemes' => [
                    'bearerAuth' => [
                        'type' => 'http',
                        'scheme' => 'bearer',
                    ]
                ],
            ],
        ];
    }
}
