<?php

declare(strict_types = 1);

namespace Lti\Lib\Config\Platform;

use Cake\Core\Configure;

class ConfigGenericPlatformLti implements PlatformConfiguration
{
    // ### PLATFORM -> TOOL ### To be generated from the platform and stored in the tool
    //Platform ID/URL: https://academy.example.com/api/v3/lti/platform
    //Public keyset URL: https://academy.example.com/api/v3/lti/platform/jwks
    //Access token URL: https://academy.example.com/api/v3/lti/platform/token
    //Authentication request URL: https://academy.example.com/api/v3/lti/platform/login

    // ### PLATFORM -> TOOL & PLATFORM ### To be generated from the platform and stored in both tool and platform
    // Platform.LTI.clientId = '4d38319a-bb92-4620-8867-ddf75bd115b4';
    // Platform.LTI.deploymentId = '16b981d3-2dc3-4cb8-a2b6-3252067cef92';

    // ### TOOL -> PLATFORM ### To be retrieved from the tool
    // Platform.LTI.toolLoginUrl = 'https://lti13.goodhabitz.com/login/';
    // Platform.LTI.toolRedirectionUrl = 'https://lti13.goodhabitz.com/launch/';
    // Platform.LTI.toolKeysetUrl = 'https://lti13.goodhabitz.com/jwks/';

    public static function getLoginUrl(): string
    {
        return self::readConfig('toolLoginUrl');
    }

    public static function usePostInitialization(): bool
    {
        return false;
    }

    public static function getFirstRedirectionUrl(): string
    {
        return self::readConfig('toolRedirectionUrl');
    }

    public static function getPublicKeySetUrl(): string
    {
        return self::readConfig('toolKeysetUrl');
    }

    public static function getDeploymentIDs(): array
    {
        return [self::readConfig('deploymentId')];
    }

    public static function getClientID(): string
    {
        return self::readConfig('clientId');
    }

    public static function getTargetLinkUrl(): string
    {
        return self::getLoginUrl();
    }

    protected static function readConfig(string $name): string
    {
        if (class_exists(\App\Lib\Helpers\Configure::class)) {
            return (string)\App\Lib\Helpers\Configure::read('Platform.LTI.' . $name);
        } else {
            return (string)Configure::read('LTI.' . $name);
        }
    }
}
