<?php

namespace Lti\Lib\LTIE;

use IMSGlobal\LTI\Cache;
use IMSGlobal\LTI\Cookie;
use IMSGlobal\LTI\Database;
use IMSGlobal\LTI\LTI_Assignments_Grades_Service;
use IMSGlobal\LTI\LTI_Message_Launch;
use IMSGlobal\LTI\LTI_Names_Roles_Provisioning_Service;

class LtiMessageLaunch extends LTI_Message_Launch
{
    public static function new(Database $database, Cache $cache = null, Cookie $cookie = null)
    {
        return new LtiMessageLaunch($database, $cache, $cookie);
    }

    public function validate(array $request = null): LTI_Message_Launch
    {
        return parent::validate($request);
    }

    protected function fileGetContents($key_set_url)
    {
        return LtiServiceConnector::fileGetContents($key_set_url);
    }

    public function get_nrps()
    {
        return new LTI_Names_Roles_Provisioning_Service(
            new LtiServiceConnector($this->registration),
            $this->jwt['body']['https://purl.imsglobal.org/spec/lti-nrps/claim/namesroleservice']);
    }

    public function get_ags()
    {
        return new LTI_Assignments_Grades_Service(
            new LtiServiceConnector($this->registration),
            $this->jwt['body']['https://purl.imsglobal.org/spec/lti-ags/claim/endpoint']);
    }
}
