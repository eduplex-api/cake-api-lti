<?php

namespace Lti\Lib\LTIE;

use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Http\Exception\InternalErrorException;
use Firebase\JWT\JWT;
use IMSGlobal\LTI\LTI_Registration;
use IMSGlobal\LTI\LTI_Service_Connector;

class LtiServiceConnector extends LTI_Service_Connector
{
    private $registration;
    private $access_tokens = [];

    public function __construct(LTI_Registration $registration)
    {
        $this->registration = $registration;
    }

    private static function getClient()
    {
        $options = ['timeout' => 10];
        if (Configure::read('debug')) {
            $options['ssl_verify_host'] = false;
            $options['ssl_verify_peer'] = false;
        }
        return new Client($options);
    }

    public static function fileGetContents(string $key_set_url): string
    {
        $client = LtiServiceConnector::getClient();
        $res = $client->get($key_set_url);
        if (!$res->isSuccess()) {
            throw new InternalErrorException(
                'Error getting ' . $key_set_url . ' ' . $res->getStringBody(), $res->getStatusCode());
        }
        return $res->getStringBody();
    }

    public function get_access_token($scopes)
    {
        // Don't fetch the same key more than once.
        sort($scopes);
        $scope_key = md5(implode('|', $scopes));
        if (isset($this->access_tokens[$scope_key])) {
            return $this->access_tokens[$scope_key];
        }

        // Build up JWT to exchange for an auth token
        $client_id = $this->registration->get_client_id();
        $jwt_claim = [
            'iss' => $client_id,
            'sub' => $client_id,
            'aud' => $this->registration->get_auth_server(),
            'iat' => time() - 5,
            'exp' => time() + 60,
            'jti' => 'lti-service-token' . hash('sha256', random_bytes(64))
        ];

        // Sign the JWT with our private key (given by the platform on registration)
        $jwt = JWT::encode($jwt_claim, $this->registration->get_tool_private_key(),
            'RS256', $this->registration->get_kid());

        // Build auth token request headers
        $auth_request = [
            'grant_type' => 'client_credentials',
            'client_assertion_type' => 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
            'client_assertion' => $jwt,
            'scope' => implode(' ', $scopes)
        ];

        // Make request to get auth token
        $client = $this->getClient();
        $url = $this->registration->get_auth_token_url();
        $res = $client->post($url, $auth_request);
        if (!$res->isSuccess()) {
            throw new InternalErrorException(
                'Error on post ' . $url . ' ' . $res->getStringBody(),
                $res->getStatusCode());
        }
        $token_data = $res->getJson();
        if (!isset($token_data['access_token']) || !isset($token_data['token_type'])) {
            throw new InternalErrorException('Error on post response ' . $url . ' ' . $res->getStringBody());
        }

        return $this->access_tokens[$scope_key] = $token_data['token_type'] . ' ' . $token_data['access_token'];
    }

    public function make_service_request(
        $scopes,
        $method,
        $url,
        $body = null,
        $content_type = 'application/json',
        $accept = 'application/json'
    ) {
        $client = $this->getClient();
        $headers = [
            'Authorization' => $this->get_access_token($scopes),
            'Accept' => $accept,
        ];
        if ($method === 'POST') {
            $headers['Content-Type'] = $content_type;
            $res = $client->post($url, $body, ['headers' => $headers]);
        } else {
            $res = $client->get($url, [], ['headers' => $headers]);
        }
        if (!$res->isSuccess()) {
            throw new InternalErrorException('Error making request ' . $url . ' ' . $res->getStringBody());
        }
        $jsonBody = $res->getJson();
        if (!$jsonBody) {
            throw new InternalErrorException('Error in response from ' . $url . ' ' . $res->getStringBody());
        }
        $headers = [];
        foreach ($res->getHeaders() as $key => $header) {
            if (isset($header[0])) {
                $header = $header[0];
            }
            $headers[$key] = $header;
        }
        return [
            'headers' => $headers,
            'body' => $jsonBody,
        ];
    }
}
