<?php

declare(strict_types = 1);

namespace Lti\Controller;

use App\Controller\ApiController;
use Lti\LtiPlugin;
use RestApi\Lib\Oauth\ScopeRules;

class PlatformLineitemsController extends ApiController
{
    public function scopeRules(): ?ScopeRules
    {
        $rules = new ScopeRules();
        $rules->setDefaultRule('https://purl.imsglobal.org/spec/lti-ags/scope/lineitem');
        return $rules;
    }

    protected function getList()
    {
        $this->flatResponse = true;
        $this->return = [
            [
                'id' => PlatformLoginController::host() . '/'.LtiPlugin::PLATFORM_AGS.'?tag=time',
                'scoreMaximum' => 999,
                'label' => 'Time',
                'tag' => 'time',
                'startDateTime' => '',
                'endDateTime' => '',
                'resourceId' => 'time'.PlatformLoginController::RESOURCE_ID
            ],
            [
                'id' => PlatformLoginController::host() . '/'.LtiPlugin::PLATFORM_AGS.'?tag=score',
                'scoreMaximum' => 108,
                'label' => 'Score',
                'tag' => 'score',
                'startDateTime' => '',
                'endDateTime' => '',
                'resourceId' => PlatformLoginController::RESOURCE_ID
            ]
        ];
    }
}
