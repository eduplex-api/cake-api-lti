<?php

declare(strict_types = 1);

namespace Lti\Controller;

use App\Controller\ApiController;
use Lti\Lib\Config\PlatformConfig;
use Lti\LtiPlugin;

class PlatformIframeController extends ApiController
{
    public const TARGET_LINK_PARAM = 'target_link_uri';// https://www.imsglobal.org/spec/lti/v1p3#target-link-uri

    public function isPublicController(): bool
    {
        return true;
    }

    protected function getList()
    {
        $fileIdInThePlatformAsMessageHint = '2';
        $loginHint = '12345'; // user-ovosplay
        $config = PlatformConfig::getconfig($this->getRequest()->getQuery('platform'));
        $targetUri = $config->getFirstRedirectionUrl();
        $loginUrl = $config->getLoginUrl();
        $target_link_uri = self::TARGET_LINK_PARAM;
        $params = [
            'iss' => PlatformLoginController::iss(),
            'login_hint' => $loginHint,
            'lti_deployment_id' => $config->getDeploymentIDs()[0],
            'client_id' => $config->getClientID(),
            $target_link_uri => $targetUri,
            'lti_message_hint' => $fileIdInThePlatformAsMessageHint,
            'lti1p3_new_window' => 1
        ];
        if ($config->usePostInitialization()) {
            $params[PlatformPostToolController::FORM_ACTION_PARAM] = $loginUrl;
            $loginUrl = PlatformLoginController::host() . '/' . LtiPlugin::PLATFORM_POST_TOOL;
        }
        $src = $loginUrl.'?'.http_build_query($params);
        $toReturn = [
            'platforms' => [],
            'iframeSrc' => $src,
        ];
        foreach (PlatformConfig::getPlatformNames() as $key => $value) {
            $toReturn['platforms'][] = $key;
        }
        if ($this->getRequest()->accepts('application/json')) {
            $this->return = $toReturn;
        } else {
            $this->response = $this->response->withStringBody($this->_getHtml($toReturn));
            $this->return = $this->response;
        }
    }

    private function _getHtml(array $toReturn): string
    {
        $html = '<div style="display: inline-block;">';
        foreach ($toReturn['platforms'] as $key) {
            $html .= '<a href="./platform?platform=' . $key . '"><h3>' . $key . '</h3></a>';
        }
        $html .= '</div>
        <iframe id="frame" src="' . $toReturn['iframeSrc'] . '" style="width:1200px; height:600px"></iframe>
        <h3><a href="' . $toReturn['iframeSrc'] . '" target="myLoginTab">Initiate login flow in a new window</a></h3>';
        return $html;
    }
}
