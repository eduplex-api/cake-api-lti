<?php

declare(strict_types = 1);

namespace Lti\Controller;

use App\Controller\ApiController;

class ToolScoreController extends ApiController
{
    public function isPublicController(): bool
    {
        return true;
    }

    protected function getList()
    {
        $this->flatResponse = true;
        $this->return = [
            'success' => true,
            'query' => $this->request->getQueryParams(),
        ];
    }
}
