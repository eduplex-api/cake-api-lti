<?php

declare(strict_types = 1);

namespace Lti\Controller;

use App\Controller\ApiController;
use Cake\Http\Exception\BadRequestException;
use IMSGlobal\LTI\LTI_OIDC_Login;
use Lti\Lib\LTIE\CacheWrapper;
use Lti\Lib\ToolDatabase;
use Lti\Lib\LTIE\Cookie;

class ToolLoginController extends ApiController
{
    public function isPublicController(): bool
    {
        return true;
    }

    protected function getList()
    {
        $request = $this->request->getQueryParams();
        $launchUrl = $request[PlatformIframeController::TARGET_LINK_PARAM] ?? null;
        if (!$launchUrl) {
            throw new BadRequestException('Target link param is mandatory');
        }
        $redirect = LTI_OIDC_Login::new(new ToolDatabase(), new CacheWrapper(), new Cookie())
            ->do_oidc_login_redirect($launchUrl, $request);
        $this->redirect($redirect->get_redirect_url());
    }
}
