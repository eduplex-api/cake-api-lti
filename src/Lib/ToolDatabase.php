<?php

namespace Lti\Lib;

use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Firebase\JWT\JWK;
use IMSGlobal\LTI\Database;
use IMSGlobal\LTI\LTI_Deployment;
use IMSGlobal\LTI\LTI_Registration;
use Lti\Controller\PlatformLoginController;
use Lti\Lib\Config\Platform\ConfigDemoGame;
use Lti\Lib\Config\PlatformConfig;
use Lti\Lib\LTIE\LtiServiceConnector;
use Lti\LtiPlugin;

class ToolDatabase implements Database
{
    public const TOOL_JWT_KEY_ID = '7c59cfe9-8c78-478e-bb7d-ecf0d99a7cba';//'58f36e10-c1c1-4df0-af8b-85c857d1634f';
    //private const TOOL_JWT_ALGO = 'RS256';

    public static function getToolHost(): string
    {
        $domain = $_SERVER['HTTP_HOST'] ?? 'host';
        $routePath = '/api/v3';
        //$routePath = LtiPlugin::getRoutePath();
        return 'https://' . $domain . $routePath . '/lti/tool';
    }

    public static function getPlatformTokenUri():string
    {
        $platformToken = LtiPlugin::PLATFORM_TOKEN;
        return PlatformLoginController::host()."/$platformToken";
    }

    //public static function getPubKey()
    //{
    //    $key_set_url = ConfigDemoGame::getPublicKeySetUrl();
    //    $keyResponse = LtiServiceConnector::fileGetContents($key_set_url);
    //    $public_key_set = json_decode($keyResponse, true);

    //    if (!$public_key_set) {
    //        throw new InternalErrorException('Failed to fetch public key');
    //    }

    //    foreach ($public_key_set['keys'] as $key) {
    //        if ($key['kid'] == ToolDatabase::TOOL_JWT_KEY_ID && $key['alg'] == ToolDatabase::TOOL_JWT_ALGO) {
    //            return openssl_pkey_get_details(JWK::parseKey($key));
    //        }
    //    }
    //    throw new InternalErrorException('Unable to find public key');
    //}

    private function _getSession($iss)
    {
        $platformLogin = LtiPlugin::PLATFORM_LOGIN;
        $jwksLogin = LtiPlugin::PLATFORM_JWKS;
        if ($iss === PlatformLoginController::iss()) {
            return [
                'client_id' => ConfigDemoGame::TOOL_CLIENT_ID,
                'auth_login_url' => PlatformLoginController::host()."/$platformLogin/",
                'auth_token_url' => self::getPlatformTokenUri(),
                'key_set_url' => PlatformLoginController::host()."/$jwksLogin/",
                'private_key_file' => '/private.key',
                'kid' => self::TOOL_JWT_KEY_ID,
                'auth_server' => null,
                'deployment' => [ConfigDemoGame::DEPLOYMENT_ID]
            ];
        }
        throw new BadRequestException('Invalid ISS');
        // return $_SESSION['iss'][$iss];
    }

    public function find_registration_by_issuer($iss)
    {
        $session = $this->_getSession($iss);
        if (!$session) {
            return false;
        }
        // http://localhost:9001/login.php?iss=http://localhost:9001&login_hint=12345&target_link_uri=http://localhost/game.php&lti_message_hint=12345
        // http://localhost:9001/platform/login.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=d42df408-70f5-4b60-8274-6c98d3b9468d&redirect_uri=http%3A%2F%2Flocalhost%3A9001%2Fgame.php&state=state-663b995cd87663_18986231&nonce=nonce-663b995cd87787.59149719&login_hint=12345&lti_message_hint=12345
        return LTI_Registration::new()
            ->set_auth_login_url($session['auth_login_url'])
            ->set_auth_token_url($session['auth_token_url'])
            ->set_auth_server($session['auth_server'])
            ->set_client_id($session['client_id'])
            ->set_key_set_url($session['key_set_url'])
            ->set_kid($session['kid'])
            ->set_issuer($iss)
            ->set_tool_private_key($this->private_key($iss));
    }

    public function find_deployment($iss, $deployment_id)
    {
        if (!in_array($deployment_id, $this->_getSession($iss)['deployment'])) {
            return false;
        }
        return LTI_Deployment::new()
            ->set_deployment_id($deployment_id);
    }

    private function private_key($iss)
    {
        // return file_get_contents(__DIR__ . $this->_getSession($iss)['private_key_file']);
        return PrivateKey::private_key($iss);
    }
}
