# LTI setup

Some settings need to be exchanged manually between Platform and Tool.

Settings provided from the tool:

* Tool URL: https://www.example.com/launch/
* Initiate login URL `Platform.LTI.toolLoginUrl`: https://www.example.com/login/
* Redirection URL(S) `Platform.LTI.toolRedirectionUrl`: https://www.example.com/launch/
* Public Keyset URL `Platform.LTI.toolKeysetUrl`: https://www.example.com/jwks/

Settings provided from the Platform:

* Platform ID/URL: https://www.example.com/api/v3/lti/platform
* Client ID `Platform.LTI.clientId`: xxxxxxxx-yyyy-xxxx-yyyy-xxxxxxxxxxxx
* Deployment ID `Platform.LTI.deploymentId`: xxxxxxxx-yyyy-xxxx-yyyy-xxxxxxxxxxxx
* Public keyset URL https://www.example.com/api/v3/lti/platform/jwks
* Access token URL https://www.example.com/api/v3/lti/platform/token
* Authentication request URL https://www.example.com/api/v3/lti/platform/login

In the Platform Config settings, we should store the config values, to be accessible via `Configure::read('Platform.LTI.<…>')`.

An RSA private key must be generated and stored in the .env `RSA_PRIVATE_KEY_BASE64`:

```
openssl ecparam -name prime256v1 -genkey -noout -out private.key
openssl ec -in private.key -pubout -out public.pem
echo "RSA_PRIVATE_KEY_BASE64=" && base64 -w 9999 private.key
```
