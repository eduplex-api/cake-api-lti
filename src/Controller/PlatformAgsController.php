<?php

declare(strict_types = 1);

namespace Lti\Controller;

use App\Controller\ApiController;
use Cake\Http\Exception\NotImplementedException;

/**
 * Assignment and Grade Services
 *
 * The Learning Tools Interoperability® (LTI®) Assignment and Grade Services
 * specification, as described in this document, replaces the Basic Outcomes
 * service and updates the Result service included in LTI v2.0. This
 * specification also allows tools more control over the number of gradebook
 * columns per resource link and the maximum points possible for each column.
 */
class PlatformAgsController extends ApiController
{
    public function isPublicController(): bool
    {
        return true;
    }

    protected function getList()
    {
        $path = explode('/', $this->request->getPath());
        $lastInPath = array_pop($path);

        $tag = $_REQUEST['tag'];
        switch ($lastInPath) {
            case 'scores':
                throw new NotImplementedException('Not implemented in the demo');
                /*
                $data = file_get_contents('php://input');
                debug($data);
                $score = json_decode($data, true);
                if ($tag == 'score') {
                    file_put_contents(__DIR__ . '/ags/score.txt', $score['scoreGiven']);
                } else {
                    file_put_contents(__DIR__ . '/ags/time.txt', $score['scoreGiven']);
                }
                $_SESSION['my_test'] = 3;
                debug($score);
                echo $data;
                break;
                //*/
            case 'results':
                if ($tag == 'score') {
                    $toRet = [
                        [
                            'id' => 'https://lms.example.com/context/2923/lineitems/1/results/5323497',
                            'userId' => PlatformNrpsController::USER_ID1,
                            'resultScore' => 0,//file_get_contents(__DIR__ . '/ags/score.txt') ?: 0,
                            'resultMaximum' => 108,
                        ],
                        [
                            'id' => 'https://lms.example.com/context/2923/lineitems/1/results/5323497',
                            'userId' => PlatformNrpsController::UID2,
                            'resultScore' => 60,
                            'resultMaximum' => 108,
                        ]
                    ];
                } else {
                    $toRet = [
                        [
                            'id' => 'https://lms.example.com/context/2923/lineitems/1/results/5323497',
                            'userId' => PlatformNrpsController::USER_ID1,
                            'resultScore' => 0,//file_get_contents(__DIR__ . '/ags/time.txt') ?: 0,
                            'resultMaximum' => 999,
                        ],
                        [
                            'id' => 'https://lms.example.com/context/2923/lineitems/1/results/5323497',
                            'userId' => PlatformNrpsController::UID2,
                            'resultScore' => 82,
                            'resultMaximum' => 999,
                        ]
                    ];
                }
                break;
        }
        $this->flatResponse = true;
        $this->return = $toRet;
    }
}
