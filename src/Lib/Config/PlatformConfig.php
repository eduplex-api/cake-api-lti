<?php

namespace Lti\Lib\Config;

use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\NotFoundException;
use Lti\Lib\Config\Platform\ConfigDemoGame;
use Lti\Lib\Config\Platform\ConfigGoodHabitz;
use Lti\Lib\Config\Platform\ConfigGenericPlatformLti;
use Lti\Lib\Config\Platform\ConfigOvosDemo;
use Lti\Lib\Config\Platform\ConfigPanoptoFhtlc;
use Lti\Lib\Config\Platform\PlatformConfiguration;

class PlatformConfig
{
    private $platformConfig = [
        'issuer_id' => 'https://proto.eduplex.eu/api/v3/lti/platform',
        'auth_url' => 'https://proto.eduplex.eu/api/v3/lti/platform/login',
        'access_token_url' => 'https://proto.eduplex.eu/api/v3/lti/platform/token',
        'keyset_url' => 'https://proto.eduplex.eu/api/v3/lti/platform/jwks',
    ];

    public static function failIfHasDbConfig(string $msg = ''): void
    {
        if (PlatformConfig::getPlatformClientId()) {
            throw new BadRequestException('Only allowed without config in database. ' . $msg);
        }
    }

    public static function getPlatformClientId(): ?string
    {
        $clientId = ConfigGenericPlatformLti::getClientID();
        if (!$clientId) {
            return null;
        }
        return $clientId;
    }

    public static function getPlatformFromConfig(): ?PlatformConfiguration
    {
        $clientId = self::getPlatformClientId();
        if (!$clientId) {
            return null;
        }
        return self::getConfigFromClientId($clientId);
    }

    public static function isToolClientIdAllowed(string $clientId): bool
    {
        $res = in_array($clientId, array_keys(self::_allConfigs()));
        if ($res) {
            return true;
        }
        return !!self::getPlatformClientId();
    }

    public static function hasAntiSpecRedirectionUrlAsGet(string $clientId)
    {
        return $clientId === ConfigDemoGame::TOOL_CLIENT_ID;
    }

    public static function getConfig(string $humanName = null): PlatformConfiguration
    {
        $config = self::getPlatformNames()[$humanName] ?? null;
        if (!$config) {
            $config = self::getPlatformNames()['Demo'] ?? null;
        }
        return $config;
    }

    public static function getConfigFromClientId(string $clientId = null): PlatformConfiguration
    {
        return self::_getConfiguration($clientId);
    }

    public static function getPlatformNames(): array
    {
        PlatformConfig::failIfHasDbConfig('Getting demo platform names');
        return [
            'Demo' => new ConfigDemoGame(),
            'GoodHabitz' => new ConfigGoodHabitz(),
            'Ovos' => new ConfigOvosDemo(),
            'Panopto' => new ConfigPanoptoFhtlc(),
        ];
    }

    private static function _allConfigs(): array
    {
        return [
            ConfigDemoGame::TOOL_CLIENT_ID => new ConfigDemoGame(),
            ConfigOvosDemo::getClientID() => new ConfigOvosDemo(),
            ConfigGoodHabitz::getClientID() => new ConfigGoodHabitz(),
            ConfigPanoptoFhtlc::getClientID() => new ConfigPanoptoFhtlc(),
        ];
    }

    public static function getFirstDeploymentId(string $clientId): string
    {
        return self::_getConfiguration($clientId)->getDeploymentIDs()[0];
    }

    private static function _getConfiguration(string $clientId): PlatformConfiguration
    {
        $map = self::_allConfigs();
        /** @var PlatformConfiguration $config */
        $config = $map[$clientId] ?? null;
        if (!isset($config)) {
            return new ConfigGenericPlatformLti();
        }
        return $config;
    }
}
