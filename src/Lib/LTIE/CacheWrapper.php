<?php

namespace Lti\Lib\LTIE;

use App\Lib\Consts\CacheGrp;
use Cake\Cache\Cache;
use Cake\Http\Exception\InternalErrorException;

class CacheWrapper extends \IMSGlobal\LTI\Cache
{
    private $cache;

    public function get_launch_data($key)
    {
        $this->load_cache();
        return $this->cache[$key];
    }

    public function cache_launch_data($key, $jwt_body)
    {
        $this->cache[$key] = $jwt_body;
        $this->save_cache();
        return $this;
    }

    public function cache_nonce($nonce)
    {
        $this->cache['nonce'][$nonce] = true;
        $this->save_cache();
        return $this;
    }

    public function check_nonce($nonce)
    {
        $this->load_cache();
        if (!isset($this->cache['nonce'][$nonce])) {
            return false;
        }
        return true;
    }

    private function load_cache()
    {
        $cache = $this->_readCache();
        if (empty($cache)) {
            $cache = '{}';
            $this->_writeCache($cache);
        }
        $this->cache = json_decode($cache, true);
    }

    private function save_cache()
    {
        $this->_writeCache(json_encode($this->cache));
    }

    private function _getCacheKey(): string
    {
        return 'LTI_cache';
        //return TMP . 'LTI_cache' . '.txt';
    }

    private function _writeCache(string $json_encode): void
    {
        Cache::write($this->_getCacheKey(), $json_encode, CacheGrp::EXTRALONG);
        //file_put_contents($this->_getCacheKey(), $json_encode);
    }

    private function _readCache()
    {
        //return file_get_contents($this->_getCacheKey());
        return Cache::read($this->_getCacheKey(), CacheGrp::EXTRALONG);
    }
}
