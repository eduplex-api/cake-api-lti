<?php

declare(strict_types = 1);

namespace Lti\Controller;

use App\Controller\ApiController;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use IMSGlobal\LTI\LTI_Lineitem;
use Lti\Lib\LTIE\CacheWrapper;
use Lti\Lib\LTIE\LtiMessageLaunch;
use Lti\Lib\ToolDatabase;

class ToolScoreboardController extends ApiController
{
    public function isPublicController(): bool
    {
        return true;
    }

    protected function getList()
    {
        $launchId = $this->getRequest()->getQuery('launch_id');
        if (!$launchId) {
            throw new BadRequestException('Param launch_id is required');
        }
        $launch = LtiMessageLaunch::from_cache($launchId, new ToolDatabase(), new CacheWrapper());
        if (!$launch->has_nrps()) {
            throw new InternalErrorException("Don't have names and roles!");
        }
        if (!$launch->has_ags()) {
            throw new InternalErrorException("Don't have grades!");
        }
        $ags = $launch->get_ags();

        $resLinkId = $launch->get_launch_data()['https://purl.imsglobal.org/spec/lti/claim/resource_link']['id'] ?? '';
        $score_lineitem = LTI_Lineitem::new()
            ->set_tag('score')
            ->set_score_maximum(100)
            ->set_label('Score')
            ->set_resource_id($resLinkId);
        $scores = $ags->get_grades($score_lineitem);

        $time_lineitem = LTI_Lineitem::new()
            ->set_tag('time')
            ->set_score_maximum(999)
            ->set_label('Time Taken')
            ->set_resource_id('time'. $resLinkId);
        $times = $ags->get_grades($time_lineitem);

        $members = $launch->get_nrps()->get_members();

        $scoreboard = [];

        foreach ($scores as $score) {
            $result = ['score' => $score['resultScore']];
            foreach ($times as $time) {
                if ($time['userId'] === $score['userId']) {
                    $result['time'] = $time['resultScore'];
                    break;
                }
            }
            foreach ($members as $member) {
                if ($member['user_id'] === $score['userId']) {
                    $result['name'] = $member['name'];
                    break;
                }
            }
            $scoreboard[] = $result;
        }
        $this->flatResponse = true;
        $this->return = $scoreboard;
    }
}
