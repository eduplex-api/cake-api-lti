<?php

namespace Lti\Lib\Config\Platform;

class ConfigPanoptoFhtlc implements PlatformConfiguration
{
    private const TOOL_CLIENT_ID = 'e54929d9-4ade-4888-850c-fb8ca724fa45';
    private const DOMAIN = 'https://fhwientest.cloud.panopto.eu';

    public static function getLoginUrl(): string
    {
        return self::DOMAIN . '/Panopto/lti/adv/platforms/8ee315d8-50e0-421c-b51d-b187009827e9/login';
    }

    public static function usePostInitialization(): bool
    {
        return false;
    }

    public static function getFirstRedirectionUrl(): string
    {
        return self::DOMAIN . '/Panopto/LTI/LTI.aspx';
    }

    public static function getPublicKeySetUrl(): string
    {
        return self::DOMAIN . '/Panopto/oauth2/.well-known/jwks';
    }

    public static function getDeploymentIDs(): array
    {
        return ['6ce0c24c-3084-4d7d-b2c9-d9fcef0bfa27'];
    }

    public static function getClientID(): string
    {
        return self::TOOL_CLIENT_ID;
    }

    public static function getTargetLinkUrl(): string
    {
        return self::getLoginUrl();
    }
}
