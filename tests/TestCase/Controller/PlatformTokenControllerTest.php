<?php

declare(strict_types = 1);

namespace Lti\Test\TestCase\Controller;

use App\Test\Fixture\OauthAccessTokensFixture;
use Firebase\JWT\JWT;
use Lti\Controller\PlatformLoginController;
use Lti\Lib\Config\Platform\ConfigDemoGame;
use Lti\Lib\PrivateKey;
use Lti\Lib\ToolDatabase;
use Lti\LtiPlugin;
use RestApi\Model\Table\OauthAccessTokensTable;
use RestApi\TestSuite\ApiCommonErrorsTest;

class PlatformTokenControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return LtiPlugin::getRoutePath() . '/lti/platform/' . LtiPlugin::PLATFORM_TOKEN . '/';
    }

    public function testAddNew()
    {
        $_SERVER['HTTP_HOST'] = 'proto.eduplex.eu';
        //$_SERVER['HTTP_HOST'] = 'localhost';
        $data = [
            'grant_type' => 'client_credentials',
            'client_assertion_type' => 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
            'client_assertion' => $this->_getJwt(),
            'scope' => 'https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/result/read https://purl.imsglobal.org/spec/lti-ags/scope/lineitem.readonly'
        ];

        $this->post($this->_getEndpoint(), $data);
        $_SERVER['HTTP_HOST'] = 'dev.this-is-unit-test.com';

        $res = $this->assertJsonResponseOK();
        $expected = [
            'access_token' => $res['access_token'],
            'token_type' => 'bearer',
            'expires_in' => 3600,
            'scope' => $data['scope']
        ];
        $this->assertEquals($expected, $res);

        $db = OauthAccessTokensTable::load()->getAccessToken($res['access_token']);
        $this->assertEquals($data['scope'], $db['scope']);
    }

    private function _getJwt(): string
    {
        $iss = PlatformLoginController::iss();
        $jwt_claim = [
            'iss' => ConfigDemoGame::TOOL_CLIENT_ID,
            'sub' => ConfigDemoGame::TOOL_CLIENT_ID,
            'aud' => PlatformLoginController::host() . '/' . LtiPlugin::PLATFORM_TOKEN,
            'iat' => time() - 5,
            'exp' => time() + 60,
            'jti' => 'lti-service-token' . hash('sha256', random_bytes(64))
        ];

        return JWT::encode($jwt_claim, PrivateKey::private_key($iss), 'RS256', ToolDatabase::TOOL_JWT_KEY_ID);
    }

    public function testAddNew_withRealOvosShouldThrowExpired()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);

        $data = [
            'grant_type' => 'client_credentials',
            'client_assertion_type' => 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
            'client_assertion' => 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIwZmUwMTIxNS1iOTg5LTRhYmMtYjJmNS1mMTc3Yjk3YzAzMDMiLCJzdWIiOiIwZmUwMTIxNS1iOTg5LTRhYmMtYjJmNS1mMTc3Yjk3YzAzMDMiLCJhdWQiOiJodHRwczovL3Byb3RvLmVkdXBsZXguZXUvYXBpL3YzL2x0aS9wbGF0Zm9ybS90b2tlbiIsImV4cCI6MTcxODIwNjU4NiwianRpIjoiMjE3MTgyMDYyODUwOTAiLCJpYXQiOjE3MTgyMDYyODV9.HVedeOMf9dEea4XYDVIowTmhIIFAgqGeNBauo_e9J1pDwDU6sYWpPtGxGMpOOx2GjvkCVa1In5Ji36ankKJekkbMm38nIAceBqMeUVukUvICHpbbbZLzqRJBOBgPQR-Si3srBt83f1b2RKVfI0y3ENwUtwb5aGifKn6-t3jy-TDc2owfZwbbK3GBKIPaOr5O_LqjGxQJn90NoV_Gd0Ks0opSkEkTtvZVDIFlDBtzRi5pJh9tjsboTJ6iXZRPp3fApZV4GXjx68DcEnBWG4ZWNm8zJMZqpBuRaLjWI3zeg0o7y2heKGhfxhRcGyIv7xBLJgzo0VC7PGkRDvr-iYCtAdcN1Tl9Y4XFVb0zj_M18Nq05mAqn-OQ3dphoUapbBGh4SIL2nKfxifIaKN164DrU4ZRhVtzX7bG8Vib2eV7olKeB93ESCRUoD4uc_mAnahgqJ6OiBiVPHA4d31RRBxlPbS4IHy6fIm7EhkQ31d3CE7W4shqLYa6QSN4olSta-WCVMEJ-m4kYR7SyR-kbJ7PjD18m4nheFemczROzbXiEC6pxF5Ilc0Fm9Iadd24s-5KdK2eHKfLsJvQpxt7Zas_fNIsdagNT_NxDhJQEPma9UVPdKrcBtt-BKID_KDo94tetzfmIc9g9RToJVsKExPv2zcabem_mDFF-alO4whgXg8',
            'scope' => 'https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/result/read https://purl.imsglobal.org/spec/lti-ags/scope/lineitem.readonly'
        ];
        $this->post($this->_getEndpoint(), $data);

        $this->assertException('Internal Server Error', 500, 'Expired token');
    }
}
