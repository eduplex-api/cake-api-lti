<?php

declare(strict_types = 1);

namespace Lti\Controller;

use App\Controller\ApiController;
use IMSGlobal\LTI\JWKS_Endpoint;
use Lti\Lib\ToolDatabase;
use Lti\Lib\PrivateKey;

class ToolJwksController extends ApiController
{
    public function isPublicController(): bool
    {
        return true;
    }

    protected function getList()
    {
        $this->flatResponse = true;
        $keyId = ToolDatabase::TOOL_JWT_KEY_ID;
        $this->return = JWKS_Endpoint::new([
            $keyId => PrivateKey::private_key(null)
        ])->get_public_jwks();
    }
}
