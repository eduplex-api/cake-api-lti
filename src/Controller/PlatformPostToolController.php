<?php

declare(strict_types = 1);

namespace Lti\Controller;

use App\Controller\ApiController;
use Cake\Http\Client;
use Cake\Http\Exception\InternalErrorException;

class PlatformPostToolController extends ApiController
{
    public const FORM_ACTION_PARAM = 'form_action';

    public function isPublicController(): bool
    {
        return true;
    }

    protected function getList()
    {
        $data = $this->getRequest()->getQueryParams();
        $url = $data[PlatformPostToolController::FORM_ACTION_PARAM];
        unset($data[PlatformPostToolController::FORM_ACTION_PARAM]);
        $uri = explode('?', $_SERVER['REQUEST_URI'] ?? '')[0] ?? '';
        unset($data[$uri]);
        $html = '<form id="auto_submit" action="'.$url.'" method="POST" style="display: none;">';
        foreach ($data as $field => $value) {
            $html .= '<input name="'.$field.'" value="'.$value.'" style="width: 100%;" /><br>';
        }
        $html .= '<button type="submit">Submit from PlatformPostToolController</button>
        </form><script>document.getElementById("auto_submit").submit();</script>';
        $this->response = $this->response->withStringBody($html);
        $this->return = $this->response;
        //$this->redirect($this->_postAsJson($url, $data));
    }

    private function _postAsJson(string $url, array $data): string
    {
        $client = new Client();
        $postOptions = [
            'headers' => ['Content-Type' => 'application/json'],
        ];
        $res = $client->post($url, $data, $postOptions);
        if (!$res->isRedirect()) {
            throw new InternalErrorException('PlatformPostTool not redirecting: ' . $url . ' '
                . ' Body sent: ' . json_encode($data)
                . $res->getStringBody(), $res->getStatusCode());
        }
        $redirection = $res->getHeader('Location')[0] ?? null;
        if (!$redirection) {
            throw new InternalErrorException('PlatformPostTool bad Location: ' . $url . ' '
                . ' Body sent: ' . json_encode($data)
                . $res->getStringBody(), $res->getStatusCode());
        }
        return $redirection;
    }
}
