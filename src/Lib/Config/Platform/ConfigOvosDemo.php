<?php

namespace Lti\Lib\Config\Platform;

class ConfigOvosDemo implements PlatformConfiguration
{
    private const TOOL_CLIENT_ID = '0fe01215-b989-4abc-b2f5-f177b97c0303';

    public static function getLoginUrl(): string
    {
        return 'https://demo.app.ovosplay.com/api/lti/login';
    }

    public static function usePostInitialization(): bool
    {
        return true;
    }

    public static function getFirstRedirectionUrl(): string
    {
        return 'https://demo.app.ovosplay.com/api/lti/oid-cb';
    }

    public static function getPublicKeySetUrl(): string
    {
        return 'https://demo.app.ovosplay.com/api/lti/keys';
    }

    public static function getDeploymentIDs(): array
    {
        return ['a3152bd7-97f8-4a79-9a42-3f8c4e4851d6'];
    }

    public static function getClientID(): string
    {
        return self::TOOL_CLIENT_ID;
    }

    public static function getTargetLinkUrl(): string
    {
        // We combine the target_link_uri with our impersonation token.
        // The uri is either defined in the token sent to oid-cb or in the initial request to the lti/login route.
        // It seems like there is either no target_link_uri defined or a wrong one.
        // It should be set in the JWT as in https://purl.imsglobal.org/spec/lti/claim/target_link_uri
        // or in the /lti/login call as target_link_uri in the request parameters, then we are storing it in the cookie.
        // Could you try set the target_link_uri to https://demo.app.ovosplay.com/ as is the tool URL?
        return 'https://demo.app.ovosplay.com/';
    }
}
