<?php

namespace Lti\Lib\LTIE;

class Cookie extends \IMSGlobal\LTI\Cookie
{
    public function set_cookie($name, $value, $exp = 3600, $options = [])
    {
        $cookie_options = [
            'expires' => time() + $exp
        ];

        // SameSite none and secure will be required for tools to work inside iframes
        $same_site_options = [
            'samesite' => 'None',
            'path' => '/',
            'secure' => true
        ];

        setcookie($name, $value, array_merge($cookie_options, $same_site_options, $options));

        // Set a second fallback cookie in the event that "SameSite" is not supported
        setcookie('LEGACY_' . $name, $value, array_merge($cookie_options, $options));
        return $this;
    }
}
