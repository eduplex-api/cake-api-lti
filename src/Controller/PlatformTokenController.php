<?php

declare(strict_types = 1);

namespace Lti\Controller;

use App\Controller\ApiController;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use \Firebase\JWT;
use Lti\Lib\AccessToken;
use Lti\Lib\Config\PlatformConfig;
use Lti\Lib\LTIE\LtiServiceConnector;
use Lti\Lib\ToolDatabase;
use RestApi\Model\Table\OauthAccessTokensTable;

/**
 * Returns valid oauth token
 *
 * @property OauthAccessTokensTable $OauthAccessTokens
 */
class PlatformTokenController extends ApiController
{
    public function isPublicController(): bool
    {
        return true;
    }

    public function initialize(): void
    {
        parent::initialize();
        $this->OauthAccessTokens = OauthAccessTokensTable::load();
    }

    protected function addNew($data)
    {
        if (($data['grant_type'] ?? null) !== 'client_credentials') {
            throw new BadRequestException('Invalid grant_type');
        }
        $jwt = $this->_validateAndDecodeJwt($data['client_assertion'] ?? '');

        if (!PlatformConfig::isToolClientIdAllowed($jwt->iss)) {
            throw new BadRequestException('Invalid tool client_id as iss ' . $jwt->iss);
        }
        if ($jwt->aud !== ToolDatabase::getPlatformTokenUri()) {
            throw new BadRequestException('Jwt was issued for another platform ' . $jwt->aud
                . ' -> ' . ToolDatabase::getPlatformTokenUri());
        }
        $this->flatResponse = true;
        $_accessToken = new AccessToken($this->OauthAccessTokens);
        $this->return = $_accessToken->storeAccessToken($data['scope']);
    }

    private function _validateAndDecodeJwt(string $jwt)
    {
        if (!$jwt) {
            throw new BadRequestException('JWT client_assertion is needed');
        }
        $explodedJwt = explode('.', $jwt);
        $iss = $this->_decode($explodedJwt[1] ?? '', 'iss');
        $kid = $this->_decode($explodedJwt[0] ?? '', 'kid');
        $pubKeys = $this->_getPubKeys($iss);
        foreach ($pubKeys as $i => $key) {
            if ($kid && $kid !== $key['kid']) {
                continue;
            }
            /** @var JWT\Key $fbKey */
            $fbKey = JWT\JWK::parseKey($key);
            $pubKey = openssl_pkey_get_details($fbKey);
            try {
                $alg = [$key['alg']];
                return JWT\JWT::decode($jwt, $pubKey['key'], $alg);
            } catch (JWT\SignatureInvalidException $e) {
            }
        }
        throw new InternalErrorException('Unable to find public key or invalid JWT signature');
    }

    private function _getPubKeys(string $clientId)
    {
        $key_set_url = PlatformConfig::getConfigFromClientId($clientId)->getPublicKeySetUrl();
        $keyResponse = LtiServiceConnector::fileGetContents($key_set_url);
        $public_key_set = json_decode($keyResponse, true);

        if (!isset($public_key_set['keys'][0])) {
            throw new InternalErrorException('Failed to fetch public keys');
        }
        return $public_key_set['keys'];
    }

    private function _decode(string $value, string $key): ?string
    {
        return json_decode(base64_decode($value), true)[$key] ?? null;
    }
}
