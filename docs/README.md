# LTI Connector

[LTI](https://www.imsglobal.org/spec/lti/v1p3#platforms-and-tools) implementation

## Works with

CakePHP Plugin [repository](https://gitlab.com/eduplex-api/cake-api-lti) to run on top of [cake-rest-api](https://packagist.org/packages/freefri/cake-rest-api).

## Openapi documentation

Swagger UI in [/api/v3/lti/openapi/](https://proto.eduplex.eu/api/v3/lti/openapi/)

## Demo Tool workflow

Here is a flowchart of the launch process:

```mermaid
sequenceDiagram
    title LTI demo launch flow
    participant User as User
    participant Platform as LTI Platform<br>(Tool Consumer)
    participant Tool as LTI Tool<br>(Tool Provider)
    User->>Platform: Access Tool via iframe
    Platform->>Tool: Render iframe in loginUrl
    Note right of Tool: Params: <br> iss <br> lti_deployment_id <br> client_id <br> target_link_uri (redirect URI)
    Tool->>Tool: do_oidc_login_redirect()
    Tool->>Platform: Redirect to Platform loginUrl(client_id, redirect_uri)
    Note left of Platform: Params: <br> scope <br> response_type <br> response_mode <br> prompt <br> client_id <br> redirect_uri <br> state <br> nonce
    Platform->>Tool: Post form to redirect_uri
    Note right of Tool: Params: <br> state <br> id_token (JWT)
    Tool->>Tool: Validate JWT (LtiMessageLaunch)
```

## License

The source code for the site is licensed under the [**MIT license**](https://gitlab.com/eduplex-api), which you can find in the [LICENSE](../LICENSE/) file.
