<?php

declare(strict_types = 1);

namespace Lti\Test\TestCase\Controller;

use Lti\Controller\PlatformIframeController;
use Lti\Lib\Config\Platform\ConfigDemoGame;
use Lti\LtiPlugin;
use RestApi\TestSuite\ApiCommonErrorsTest;

class ToolLoginControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
    ];

    protected function _getEndpoint(): string
    {
        return LtiPlugin::getRoutePath() . '/lti/tool/' . LtiPlugin::TOOL_LOGIN . '/';
    }

    public function testGetList_shouldThrowException()
    {
        $urlParams = [
            //'iss' => 'http://localhost:9001',// PlatformLoginController::ISS
            'login_hint' => '12345',
            PlatformIframeController::TARGET_LINK_PARAM => ConfigDemoGame::getFirstRedirectionUrl(),
            'lti_message_hint' => '12345'
        ];
        $params = '?' . urldecode(http_build_query($urlParams));
        $this->get($this->_getEndpoint() . $params);

        $this->assertException('Internal Server Error', 500, 'Could not find issuer');
    }
}
