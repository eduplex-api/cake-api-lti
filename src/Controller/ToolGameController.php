<?php

declare(strict_types = 1);

namespace Lti\Controller;

use App\Controller\ApiController;
use Cake\Http\Exception\NotImplementedException;
use Lti\Lib\LTIE\CacheWrapper;
use Lti\Lib\LTIE\Cookie;
use Lti\Lib\LTIE\LtiMessageLaunch;
use Lti\Lib\ToolDatabase;
use Lti\LtiPlugin;

class ToolGameController extends ApiController
{
    public function isPublicController(): bool
    {
        return true;
    }

    protected function getList()
    {
        // This is not standard should throw not implement
        $this->_renderGame($this->getRequest()->getQueryParams());
    }

    protected function addNew($data)
    {
        $this->_renderGame($data);
    }

    private function _renderGame(array $request)
    {
        $launch = LtiMessageLaunch::new(new ToolDatabase(), new CacheWrapper(), new Cookie())
            ->validate($request);

        $s = '<link href="static/breakout.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Gugi" rel="stylesheet">';

        $launch_id = $launch->get_launch_id();
        $launch_data = $launch->get_launch_data();
        $name = $launch_data['name'] ?? '';
        $currDiff = $launch_data['https://purl.imsglobal.org/spec/lti/claim/custom']['difficulty'] ?? 'normal';
        $configureUri = ToolDatabase::getToolHost() .'/configure.php';
        if ($launch->is_deep_link_launch()) {
            $s = $s. '
            <div class="dl-config">
                <h1>Pick a Difficulty</h1>
                <ul>
                    <li><a href="' . $configureUri . '?diff=easy&launch_id='
                . $launch_id . '">Easy</a></li>
                    <li><a href="' . $configureUri . '?diff=normal&launch_id='
                . $launch_id . '">Normal</a></li>
                    <li><a href="' . $configureUri . '?diff=hard&launch_id='
                . $launch_id . '">Hard</a></li>
                </ul>
            </div>';
            throw new NotImplementedException('Deep link not implemented');
        } else {
            $s = $s . '<div id="game-screen">
                <div style="position:absolute;width:1000px;margin-left:-500px;left:50%; display:block">
                    <div id="scoreboard" style="position:absolute; right:0; width:200px; height:486px">
                        <h2 style="margin-left:12px;">Scoreboard</h2>
                        <table id="leadertable" style="margin-left:12px;">
                        </table>
                    </div>
                    <canvas id="breakoutbg" width="800" height="500" style="position:absolute;left:0;border:0;">
                    </canvas>
                    <canvas id="breakout" width="800" height="500" style="position:absolute;left:0;">
                    </canvas>
                </div>
            </div>';
        }
        $scoreboard = LtiPlugin::TOOL_SCOREBOARD;
        $score = LtiPlugin::TOOL_SCORE; //'api/score.php';
        $s = $s . '
        <script>
            // Set game difficulty if it has been set in deep linking
            var curr_diff = "' . $currDiff . '";
            var curr_user_name = "' . $name . '";
            var launch_id = "' . $launch_id . '";
            var score_req_url = "' . $score . '";
            var scoreboard_req_url = "' . $scoreboard . '/?launch_id=' . $launch_id . '";
        </script>
        <script type="text/javascript" src="static/breakout.js" charset="utf-8"></script>';
        die($s);
    }
}
