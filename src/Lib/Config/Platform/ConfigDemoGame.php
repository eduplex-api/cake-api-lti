<?php

namespace Lti\Lib\Config\Platform;

use Lti\Lib\ToolDatabase;
use Lti\LtiPlugin;

class ConfigDemoGame implements PlatformConfiguration
{
    //'d42df408-70f5-4b60-8274-6c98d3b9468d';
    public const TOOL_CLIENT_ID = 'c119ccb0-34e9-41fa-a2d8-549840b88a9c';
    //'8c49a5fa-f955-405e-865f-3d7e959e809f';
    public const DEPLOYMENT_ID = '02ecab61-79d4-43e1-85d8-a3928cfd82fb';

    public static function getLoginUrl(): string
    {
        return ToolDatabase::getToolHost() . '/' . LtiPlugin::TOOL_LOGIN . '/';
    }

    public static function usePostInitialization(): bool
    {
        return false;
    }

    public static function getFirstRedirectionUrl(): string
    {
        return ToolDatabase::getToolHost() . '/' . LtiPlugin::TOOL_REDIRECTION;//'http://localhost/game.php',
    }

    public static function getPublicKeySetUrl(): string
    {
        return ToolDatabase::getToolHost() . '/' . LtiPlugin::TOOL_JWKS;
    }

    public static function getDeploymentIDs(): array
    {
        return [self::DEPLOYMENT_ID];
    }

    public static function getClientID(): string
    {
        return self::TOOL_CLIENT_ID;
    }

    public static function getTargetLinkUrl(): string
    {
        // this seems not really used in the demo game
        return self::getLoginUrl();
    }
}
